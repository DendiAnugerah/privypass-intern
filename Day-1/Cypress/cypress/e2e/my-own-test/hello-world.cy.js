/// <referece type="cypress" />;

describe("Basic dekstop test", () => {
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.visit("https://codedamn.com");

    // cy.then(() => {
    //   window.localStorage.setItem();
    // });
  });

  it("We have correct page title", () => {
    cy.visit("https://codedamn.com");

    cy.contains("Learn Programming").should("have.text", "Learn Programming");
  });

  // Login page
  it("Login page looks good", () => {
    cy.viewport(1280, 720);
    cy.visit("https://codedamn.com");

    cy.contains("Sign In").click();
    cy.contains("sign up with").should("exist");
  });

  // Verify login page url
  it("The login page links work", () => {
    cy.viewport(1280, 720);
    cy.visit("https://codedamn.com");

    cy.contains("Sign In").click();
    cy.contains("Forgot your password?").click({ force: true });
    cy.url().should("include", "https://codedamn.com/password-reset");
    cy.go("back");
  });

  // Typing with cypress
  it("Login should work fine", () => {
    cy.viewport(1280, 720);
    cy.visit("https://codedamn.com");

    cy.contains("Sign In").click();
    cy.get("[data-testid=username]").type("admin", { force: true });
    cy.get("[data-testid=password]").type("admin", { force: true });

    cy.get("[data-testid=login]").click();
  });

  // Viewport
  it("Every basic element exist on mobile", () => {
    cy.viewport("iphone-x");
    cy.visit("https://codedamn.com");
  });
});
