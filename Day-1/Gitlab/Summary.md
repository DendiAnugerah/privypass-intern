## Git Command

| Command                            | Kegunaan                                                             |
| ---------------------------------- | -------------------------------------------------------------------- |
| `git init <options> `              | Membuat repository pada file lokal                                   |
| `git add <file> / <directory>`     | Menambah file baru pada repository yang telah kita pilih             |
| `git remote <options> `            | Menambahkan koneksi remote                                           |
| `git status `                      | Untuk mengetahui status repository tersebut                          |
| `git commit <options> `            | Menyimpan perubahan yang sudah kita lakukan                          |
| `git push <options> `              | Mengupload perubahan file yang sudah kita commit sebelumnya          |
| `git fetch <remote> [options] `    | Mengambil commit terbaru                                             |
| `git pull <remote> <branch-name> ` | Mengambil commit terbaru lalu menggabungkan dengan branch aktif kita |
| `git branch <branch> `             | Melihat branch yang ada pada repository kita                         |
| `git merge <branch-name> `         | Menggabungkan dua branch menjadi satu                                |
| `git stash <command> `             | Untuk menyimpan proggres yang sudah kita lakukan                     |
