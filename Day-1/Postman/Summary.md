# Day-1 | 12-12-2022

#### Postman Beginner Course - API Testing : https://www.youtube.com/watch?v=VywxIQ2ZXw4

#### API: https://simple-books-api.glitch.me

---

## Part 1 - Introduction to Postman

`GET` : Untuk mendapatkan data dari server.

- Untuk melakukan request, dapat dilakukan dengan cara membuat collection lalu memasukan request URL dan juga kita perlu memasukkan endpoints dari request tersebut.
- Dalam melakukan request, kita dapat menambahkan variable-variable seperti baseURL, token, dan lain-lain, yaitu pada bagian environment.
- Untuk memasukkan parameter, dapat dilakukan dengan cara menambahkan parameter pada request URL, terdapat 3 kolom yang berisi key, dan 2 kolom optional yang dapat diisi, yaitu value dan description.

`POST` : Untuk mengirim data ke server.

Untuk mengirim data terdapat request body yang berisi data-data yang akan dikirimkan ke server dalam format JSON.\
Example:

```

{
    "name": "John Doe",
    "email": "ikan@gmail.com"
}

```

`PACTH` : Untuk mengubah data yang ada di server.

Example:

```
PATCH /orders/PF6MflPDcuhWobZcgmJy5
Authorization: Bearer <YOUR TOKEN>

{
  "customerName": "IkanHiu"
}
```

`DELETE` : Untuk menghapus data yang ada di server.
Untuk melakukan DELETE, request body tidak diperlukan.

Example:

```
DELETE /orders/PF6MflPDcuhWobZcgmJy5
Authorization: Bearer <YOUR TOKEN>
```

## Part 2 - Test Automation with Postman

Untuk melakukan test automation, kita dapat menggunakan test script yang berisi script yang akan dijalankan ketika request telah selesai.\

Sebagai contoh jika kita ingin melakukan test automation untuk memastikan bahwa response status code yang diterima adalah 200, maka kita dapat menuliskan script berikut:

```
pm.test("Status code is 200", function () {
    pm.response.to.have.status(200);
});
```

Untuk menambahkan test result yang akan ditampilkan ketika test telah selesai, berdasarkan code diatas, kita dapat menambahkan beberapa script yaitu sebagai berikut:

```
pm.test("Status code is 200", function () {
    pm.response.to.have.status(200);
});

const response = pm.response.json();

console.log(response);
console.log(response['status']);

pm.test("Status should be OK", () => {
    pm.expect(response.status).to.eql("OK");
});
```

Kita juga dapat menambahkan variable yang akan digunakan, dengan cara menambahkan variable pada environment, lalu memanggil variable tersebut pada test value.

Untuk melakukan semua test secara langsung, kita dapat menggunakan Runner yang berisi semua request yang telah kita buat.
