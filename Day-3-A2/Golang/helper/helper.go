package helper

import "strings"

func ValidateUserInput(firstname, lastname, email string, userticket, remainticket int) (bool, bool, bool) {
	isValidName := len(firstname) >= 2 && len(lastname) >= 2
	isValidEmail := strings.Contains(email, "@")
	isValidTicket := userticket > 0 && userticket <= remainticket

	return isValidName, isValidEmail, isValidTicket
}
