# Day-3 | 12-14-2022

#### JavaScript Crash Course For Beginners : https://www.youtube.com/watch?v=hdI2bqOjy3c

---

## Variable and Data Types

### Variable

There are 3 ways to declare a variable in JavaScript:

- `var` : can be re-declared and updated
- `let` : can be updated but not re-declared
- `const` : cannot be re-declared or updated

### Data Types

Common data types in JavaScript:

- `String` : text
- `Number` : integer or float
- `Boolean` : true or false
- `Array` : list of data

  ```
  Example of an array:

  const fruits = ["apples", "oranges", "pears"];
  console.log(fruits[0]);
  fruits.push("Manngggoo");
  fruits.unshift("Strawberries");
  fruits.pop();
  console.log(fruits);
  ```

- `Object` : key-value pairs

  ```
  Example of an object:
  const person{
    firstName : 'Dendi',
    lastName : 'Anugerah',
    age: 20,
    hobbies: ['music', 'movies', 'sports'],
    address{
        street: 'Jl. Kebon Jeruk',
        city: 'Jakarta',
        state: 'DKI Jakarta'
    }
  }
  ```

- `Undefined` : variable that has not been assigned a value
- `Null` : empty value
- `Symbol` : unique value

### Loops and Iteration

- `for` : loops through a block of code a number of times

  ```
  Example of for loop:
  for(let i = 0; i < 10; i++){
    console.log(`For Loop Number: ${i}`);
  }
  ```

- `while` : loops through a block of code while a specified condition is true

  ```
  Example of while loop:
  let i = 0;
  while(i < 10){
      console.log(`While Loop Number: ${i}`);
      i++;
  }
  ```

- `forEach` : loops through a block of code for each element in an array

  ```
  Example of forEach loop:
  const cars = ['Ford', 'Chevy', 'Honda', 'Toyota'];
  cars.forEach(function(car, index, array){
      console.log(`${index} : ${car}`);
      console.log(array);
  });
  ```

- `Map` : loops through and creates a new array

  ```
  Example of map loop:
  const users = [
    {id: 1, name: 'Mangga'},
    {id: 2, name: 'Apel'},
    {id: 3, name: 'Hiu'},
    {id: 4, name: 'Ikan'}
  ];

  const ids = users.map(function(user){
    return user.id;
  });

  console.log(ids);
  ```

- `Filter` : loops through and creates a new array based on a condition

  ```
  Example of filter loop:
  const users = [
    {id: 1, name: 'Mangga'},
    {id: 2, name: 'Apel'},
    {id: 3, name: 'Hiu'},
    {id: 4, name: 'Ikan'}
  ];

  const user = users.filter(function(user){
    return user.id == 2;
  });

  console.log
  ```

### Condition

```
Example of if condition

const x = 10;
if(x == 10){
  console.log('x is 10');
} else if (x > 10) {
  console.log('x is greater than 10');
} else {
  console.log('x is less than 10');
}
```

Use `&&` and `||` to make a condition
also you can use shorthand in if statement

```
Example of shorthand if statement

const x = 10;
const color = x > 10 ? 'red' : 'blue';  // that means if x > 10 then color is red, else color is blue

console.log(color);
```

```
Example Switch

const x = 10;

switch (x) {
  case 10:
    console.log("Greater than 10");
    break;
  case 12:
    console.log("is 10");
    break;
}

```

### Function

Function is a block of code that can be reused

```
Example of function

function addNums(num1, num2) {
  console.log(num1 + num2);
}

addNums(1, 2);
```

### Object Oriented Programming

- `Constructor` : a function that creates an object

  ```
  Example of constructor

  function Person(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;

    this.getFullName = function () {
      return `${this.firstName} ${this.lastName}`;
    };
  }

  const person1 = new Person("Dendi", "Anugerah");
  const person2 = new Person("Ikan", "Teripang");

  console.log(person1);
  console.log(person2.getFullName());
  ```

- `Class` : a blueprint for creating objects

  ```
  Example of class

  class Person {
    constructor(firstName, lastName) {
      this.firstName = firstName;
      this.lastName = lastName;
    }

    getFullName() {
      return `${this.firstName} ${this.lastName}`;
    }
  }

  const person1 = new Person("Dendi", "Anugerah");
  const person2 = new Person("Ikan", "Teripang");

  console.log(person1);
  console.log(person2.getFullName());
  ```
