package handler

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"v1/book"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type bookHandler struct {
	bookService book.Service
}

func NewBookHandler(bookService book.Service) *bookHandler {
	return &bookHandler{bookService}
}

func (handler *bookHandler) GetBooks(c *gin.Context) {
	books, err := handler.bookService.FindAll()
	if err != nil {
		log.Panic(err)
	}

	var bookResponse []book.BookResponse
	for _, b := range books {
		bookR := convertToBookResponse(b)

		bookResponse = append(bookResponse, bookR)
	}

	c.JSON(http.StatusOK, gin.H{
		"data": bookResponse,
	})
}

func (handler *bookHandler) GetBook(c *gin.Context) {
	id := c.Param("id")
	Id, _ := strconv.Atoi(id)

	gbook, err := handler.bookService.FindById(Id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	bookResponse := convertToBookResponse(gbook)

	c.JSON(http.StatusOK, gin.H{
		"data": bookResponse,
	})
}

func (handler *bookHandler) PostBooksHandler(c *gin.Context) {
	var BookInput book.BookInput

	err := c.ShouldBindJSON(&BookInput)
	if err != nil {

		errMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errMessage := fmt.Sprintf("Error on field %s, condition: %s", e.Field(), e.ActualTag())
			errMessages = append(errMessages, errMessage)
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": errMessages,
		})
		return
	}

	book, err := handler.bookService.Create(BookInput)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": book,
	})
}

func (handler *bookHandler) UpdateBook(c *gin.Context) {
	var BookInput book.BookInput
	id := c.Param("id")
	Id, _ := strconv.Atoi(id)

	err := c.ShouldBindJSON(&BookInput)
	if err != nil {

		errMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errMessage := fmt.Sprintf("Error on field %s, condition: %s", e.Field(), e.ActualTag())
			errMessages = append(errMessages, errMessage)
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": errMessages,
		})
		return
	}

	book, err := handler.bookService.Update(Id, BookInput)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": book,
	})
}

func convertToBookResponse(gbook book.Book) book.BookResponse {
	return book.BookResponse{
		Id:          gbook.ID,
		Title:       gbook.Title,
		Description: gbook.Description,
		Price:       gbook.Price,
		Rating:      gbook.Rating,
	}
}
