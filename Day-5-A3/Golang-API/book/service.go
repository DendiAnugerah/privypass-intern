package book

type service struct {
	repository Repository
}

type Service interface {
	FindAll() ([]Book, error)
	FindById(id int) (Book, error)
	Create(book BookInput) (Book, error)
	Update(id int, book BookInput) (Book, error)
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) FindAll() ([]Book, error) {
	books, err := s.repository.FindAll()
	return books, err
}

func (s *service) FindById(id int) (Book, error) {
	book, err := s.repository.FindById(id)
	return book, err
}

func (s *service) Create(bookInput BookInput) (Book, error) {

	books := Book{
		Title:       bookInput.Title,
		Price:       bookInput.Price,
		Description: bookInput.Description,
		Rating:      bookInput.Rating,
	}

	newBook, err := s.repository.Create(books)
	return newBook, err
}

func (s *service) Update(id int, bookInput BookInput) (Book, error) {
	book, err := s.repository.FindById(id)

	book.Title = bookInput.Title
	book.Price = bookInput.Price
	book.Description = bookInput.Description
	book.Rating = bookInput.Rating

	newBook, err := s.repository.Update(book)
	return newBook, err
}
