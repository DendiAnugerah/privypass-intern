import { useScreenshot, createFileName } from "use-react-screenshot";

const Screenshot = ({ userStream }) => {
  const [image, takeScreenshot] = useScreenshot({
    type: "image/jpeg",
    quality: 1.0,
  });

  const download = (image, { name = "img", extension = "jpg" } = {}) => {
    const a = document.createElement("a");
    a.href = image;
    a.download = createFileName(extension, name);
    a.click();
  };

  const downloadScreenshot = () =>
    takeScreenshot(userStream.current).then(download);

  return (
    <div>
      <button onClick={downloadScreenshot}>Take Screenshot</button>
    </div>
  );
};

export default Screenshot;
